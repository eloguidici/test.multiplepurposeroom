﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Test.MultiplePurposeRoom.Domain.Enums;
using Test.MultiplePurposeRoom.Domain.Exceptions;
using Test.MultiplePurposeRoom.Domain.Interfaces;

namespace Test.MultiplePurposeRoom.Core.Business.Tests
{
    [TestClass()]
    public class ReserveBusinessTests
    {
        Mock<IMultiplePurposeRoomBusiness> _multiplePurposeRoomBusiness;
        Mock<IHolidayBusiness> _holidayBusiness;
        Mock<IReserveRepository> _reserveRepository;
        Mock<ICustomerBusiness> _customerBusiness;
        Mock<IUserBusiness> _userBusiness;

        [TestInitialize]
        public void Initialize()
        {
            _multiplePurposeRoomBusiness = new Mock<IMultiplePurposeRoomBusiness>();
            _holidayBusiness = new Mock<IHolidayBusiness>();
            _reserveRepository = new Mock<IReserveRepository>();
            _customerBusiness = new Mock<ICustomerBusiness>();
            _userBusiness = new Mock<IUserBusiness>();
        }

        public IReserveBusiness CreateTarget()
        {
            var target = new ReserveBusiness(_reserveRepository.Object, _multiplePurposeRoomBusiness.Object,
               _holidayBusiness.Object, _customerBusiness.Object, _userBusiness.Object);
            return target;
        }

        [ExpectedException(typeof(UserInvalidIdException))]
        [TestMethod()]
        public void Add_Reserve_With_User_Invalid_Id_Test()
        {
            var reserve = new Entity.Reserve()
            {
                Date = DateTime.UtcNow
            };

            _userBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.User, bool>>>()))
                .Returns(false)
                .Verifiable();

            var target = CreateTarget();
            target.Add(reserve);
        }

        [ExpectedException(typeof(MultiplePurposeRoomInvalidIdException))]
        [TestMethod()]
        public void Add_Reserve_With_MultiplePurposeRoom_Invalid_Id_Test()
        {
            var reserve = new Entity.Reserve()
            {
                Date = DateTime.UtcNow
            };

            _userBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.User, bool>>>()))
                .Returns(true)
                .Verifiable();

            _multiplePurposeRoomBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.MultiplePurposeRoom, bool>>>()))
                .Returns(false)
                .Verifiable();
            
            var target = CreateTarget();
            target.Add(reserve);
        }


        [ExpectedException(typeof(HolidayInvalidDateException))]
        [TestMethod()]
        public void Add_Reserve_With_Holiday_Invalid_Date_Test()
        {
            var reserve = new Entity.Reserve()
            {
                Date = DateTime.UtcNow
            };

            _userBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.User, bool>>>()))
                .Returns(true)
                .Verifiable();

            _multiplePurposeRoomBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.MultiplePurposeRoom, bool>>>()))
                .Returns(true)
                .Verifiable();

            _holidayBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.Holiday, bool>>>()))
                .Returns(true)
                .Verifiable();

            var target = CreateTarget();
            target.Add(reserve);
        }

        [ExpectedException(typeof(MultiplePurposeRoomNotAvailableException))]
        [TestMethod()]
        public void Add_Reserve_With_Multiple_Purpose_Room_NotAvailable_Test()
        {
            var reserve = new Entity.Reserve()
            {
                Date = DateTime.UtcNow
            };

            _userBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.User, bool>>>()))
                .Returns(true)
                .Verifiable();

            _multiplePurposeRoomBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.MultiplePurposeRoom, bool>>>()))
                .Returns(true)
                .Verifiable();

            _holidayBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.Holiday, bool>>>()))
                .Returns(false)
                .Verifiable();

            _multiplePurposeRoomBusiness.Setup(x => x.Get())
                .Returns(new List<Entity.MultiplePurposeRoom>()
                {
                    new Entity.MultiplePurposeRoom(){ Id = 1}
                })
                .Verifiable();

            _reserveRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.Reserve, bool>>>()))
                .Returns(false)
                .Verifiable();

            var target = CreateTarget();
            target.Add(reserve);
        }

        [TestMethod()]
        public void Add_Reserve_With_Customer_Not_Registered_Test()
        {
            var reserve = new Entity.Reserve()
            {
                Date = DateTime.UtcNow
            };

            _userBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.User, bool>>>()))
                .Returns(true)
                .Verifiable();

            _multiplePurposeRoomBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.MultiplePurposeRoom, bool>>>()))
                .Returns(true)
                .Verifiable();

            _holidayBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.Holiday, bool>>>()))
                .Returns(false)
                .Verifiable();

            _multiplePurposeRoomBusiness.Setup(x => x.Get())
                .Returns(new List<Entity.MultiplePurposeRoom>()
                {
                    new Entity.MultiplePurposeRoom(){ Id = 1}
                })
                .Verifiable();

            _reserveRepository.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.Reserve, bool>>>()))
                .Returns(true)
                .Verifiable();

            _customerBusiness.Setup(x => x.Any(It.IsAny<Expression<Func<Entity.Customer, bool>>>()))
                .Returns(false)
                .Verifiable();

            _customerBusiness.Setup(x => x.Add(It.IsAny<Entity.Customer>()))
                .Verifiable();

            _reserveRepository.Setup(x => x.Add(It.IsAny<Entity.Reserve>()))
                .Verifiable();

            var target = CreateTarget();
            target.Add(reserve);

            _userBusiness.Verify(x => x.Any(It.IsAny<Expression<Func<Entity.User, bool>>>()), Times.Once);
            _holidayBusiness.Verify(x => x.Any(It.IsAny<Expression<Func<Entity.Holiday, bool>>>()), Times.Once);
            _multiplePurposeRoomBusiness.Verify(x => x.Get(), Times.Once);
            _reserveRepository.Verify(x => x.Any(It.IsAny<Expression<Func<Entity.Reserve, bool>>>()), Times.Once);
            _customerBusiness.Verify(x => x.Any(It.IsAny<Expression<Func<Entity.Customer, bool>>>()), Times.Once);
            _customerBusiness.Verify(x => x.Add(It.IsAny<Entity.Customer>()), Times.Once);
            _reserveRepository.Verify(x => x.Add(It.IsAny<Entity.Reserve>()), Times.Once);

            Assert.IsTrue(reserve.Status == (byte)ReserveStatus.Active);

        }

    }
}