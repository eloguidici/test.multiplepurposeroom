﻿namespace Test.MultiplePurposeRoom.Domain.Enums
{
    public enum ReserveStatus : byte
    {
        Active = 1, Cancel, Finished
    }
}
