﻿namespace Test.MultiplePurposeRoom.Domain.Interfaces
{
    using Test.MultiplePurposeRoom.Entity;
    public interface IMultiplePurposeRoomRepository : IRepository<MultiplePurposeRoom>
    {

    }
}
