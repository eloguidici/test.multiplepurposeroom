﻿using Test.MultiplePurposeRoom.Entity;
namespace Test.MultiplePurposeRoom.Domain.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {

    }
}
