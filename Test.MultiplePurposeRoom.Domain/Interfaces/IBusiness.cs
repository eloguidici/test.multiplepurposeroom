﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Test.MultiplePurposeRoom.Domain.Interfaces
{
    public interface IBusiness<T>
    {
        void Add(T target);
        void Update(T target);
        List<T> Get();
        T Get(int id);
        List<T> Get(Expression<Func<T, bool>> predicate);
        bool Any(Expression<Func<T, bool>> predicate);
        T First(Expression<Func<T, bool>> predicate);
    }
}
