﻿using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Domain.Interfaces
{
    public interface ICustomerBusiness : IBusiness<Customer>
    {

    }
}
