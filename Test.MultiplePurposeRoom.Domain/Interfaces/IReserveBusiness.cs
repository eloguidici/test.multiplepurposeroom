﻿using System;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Domain.Interfaces
{
    public interface IReserveBusiness : IBusiness<Reserve>
    {
        bool Available(DateTime date, int fromHour,int toHour, byte type);
        void Cancel(int reserveId);
    }
}
