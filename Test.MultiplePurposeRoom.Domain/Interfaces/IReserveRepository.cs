﻿namespace Test.MultiplePurposeRoom.Domain.Interfaces
{
    using Test.MultiplePurposeRoom.Entity;
    public interface IReserveRepository : IRepository<Reserve>
    {

    }
}
