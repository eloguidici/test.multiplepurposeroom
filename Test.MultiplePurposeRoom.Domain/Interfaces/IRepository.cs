﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Test.MultiplePurposeRoom.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T target);
        void Update(T target);
        List<T> Get(Expression<Func<T, bool>> predicate);
        T First(Expression<Func<T, bool>> predicate);
        Boolean Any(Expression<Func<T, bool>> predicate);
        List<T> Get();
#warning "Agregar un metodo de paginado";

    }
}
