﻿using System;
using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class HolidayInvalidDateException : HttpException
    {
        public HolidayInvalidDateException() : base(Exceptions.Message.HOLIDAY_INVALID_DATE) { }

    }
}
