﻿using System;
using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class MultiplePurposeRoomNotAvailableException : HttpException
    {
        public MultiplePurposeRoomNotAvailableException() : base(Exceptions.Message.MULTIPLEPURPOSEROOM_NOT_AVAILABLE) { }

    }
}
