﻿using System;
using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class UserInvalidIdException : HttpException
    {
        public UserInvalidIdException() : base(Exceptions.Message.USER_INVALID_ID) { }
    }
}
