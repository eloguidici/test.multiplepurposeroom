﻿namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public static class Message
    {
        public const string HOLIDAY_INVALID_DATE = "Invalid holiday date.";
        public const string MULTIPLEPURPOSEROOM_NOT_AVAILABLE= "Multiple purpose room not available.";
        public const string RESERVE_INVALID_ID = "Invalid reserve id.";
        public const string RESERVE_STATUS_CANCEL = "Reserve is canceled.";
        public const string RESERVE_STATUS_FINISHED = "Reserve is finished.";
        public const string CUSTOMER_INVALID_EMAIL = "Invalid customer email.";
        public const string USER_INVALID_ID = "Invalid user id.";
        public const string MULTIPLEPURPOSEROOM_INVALID_ID = "Invalid multiple purpose room id.";
    }
}


