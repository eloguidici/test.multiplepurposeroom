﻿using System;
using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class ReserveStatusCanceledException : HttpException
    {
        public ReserveStatusCanceledException() : base(Exceptions.Message.RESERVE_STATUS_CANCEL) { }
    }
}
