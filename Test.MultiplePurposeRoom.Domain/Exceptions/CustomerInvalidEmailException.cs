﻿using System;
using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class CustomerInvalidEmailException : HttpException
    {
        public CustomerInvalidEmailException() : base(Exceptions.Message.CUSTOMER_INVALID_EMAIL) { }

    }
}
