﻿using System;
using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class MultiplePurposeRoomInvalidIdException : HttpException
    {
        public MultiplePurposeRoomInvalidIdException() : base(Exceptions.Message.MULTIPLEPURPOSEROOM_INVALID_ID) {}
    }
}
