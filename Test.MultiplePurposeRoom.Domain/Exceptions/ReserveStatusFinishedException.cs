﻿using System;
using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class ReserveStatusFinishedException : HttpException
    {
        public ReserveStatusFinishedException() : base(Exceptions.Message.RESERVE_STATUS_FINISHED) { }

    }
}
