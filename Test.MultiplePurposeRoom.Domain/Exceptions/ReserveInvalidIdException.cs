﻿using System.Web;

namespace Test.MultiplePurposeRoom.Domain.Exceptions
{
    public class ReserveInvalidIdException : HttpException
    {
        public ReserveInvalidIdException() : base(Exceptions.Message.RESERVE_INVALID_ID) { }
    }
}
