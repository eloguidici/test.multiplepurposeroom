﻿namespace Test.MultiplePurposeRoom.DTO
{
    public  class HolidayDto
    {
        public int Id { get; set; }
        public System.DateTime Date { get; set; }
        public string Description { get; set; }
    }
}
