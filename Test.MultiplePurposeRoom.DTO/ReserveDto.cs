﻿namespace Test.MultiplePurposeRoom.DTO
{
    public class ReserveDto
    {
        public System.DateTime Date { get; set; }
        public int Id { get; set; }
        public int FromHour { get; set; }
        public int UserId { get; set; }
        public int RoomId { get; set; }
        public int CustomerId { get; set; }
        public byte Status { get; set; }
        public int ToHour { get; set; }
        public System.DateTime CreatedDate { get; set; }

        public CustomerDto Customer { get; set; }
        public UserDto User { get; set; }
    }
}
