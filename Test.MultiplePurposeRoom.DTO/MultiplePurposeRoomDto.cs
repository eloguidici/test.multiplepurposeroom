﻿using System.Collections.Generic;

namespace Test.MultiplePurposeRoom.DTO
{
    public class MultiplePurposeRoomDto
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public int Capacity { get; set; }
        public decimal Dimension { get; set; }
        public MultiplePurposeRoomTypeDto MultiplePurposeRoomType { get; set; }
        public IList<MultiplePurposeRoomEquipmentDto> MultiplePurposeRoomEquipments { get; set; }
    }
}
