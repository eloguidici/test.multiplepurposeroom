﻿namespace Test.MultiplePurposeRoom.DTO
{
    public class MultiplePurposeRoomTypeDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
