﻿namespace Test.MultiplePurposeRoom.DTO
{
    public class EquipmentDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
