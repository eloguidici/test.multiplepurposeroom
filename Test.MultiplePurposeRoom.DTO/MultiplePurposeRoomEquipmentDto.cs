﻿namespace Test.MultiplePurposeRoom.DTO
{
    public class MultiplePurposeRoomEquipmentDto
    {
        public int RoomId { get; set; }
        public int EquipmentId { get; set; }
        public int Count { get; set; }

        public virtual EquipmentDto Equipment { get; set; }
    }
}
