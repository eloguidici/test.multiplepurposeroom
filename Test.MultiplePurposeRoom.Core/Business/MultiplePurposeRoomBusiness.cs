﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Core.Business
{
    public class MultiplePurposeRoomBusiness : IMultiplePurposeRoomBusiness
    {
        private IMultiplePurposeRoomRepository _multiplePurposeRoomRepository;

        public MultiplePurposeRoomBusiness(IMultiplePurposeRoomRepository multiplePurposeRoomRepository)
        {
            _multiplePurposeRoomRepository = multiplePurposeRoomRepository;
        }

        public void Add(Entity.MultiplePurposeRoom target)
        {
            throw new NotImplementedException();
        }

        public bool Any(Expression<Func<Entity.MultiplePurposeRoom, bool>> predicate)
        {
            return _multiplePurposeRoomRepository.Any(predicate);
        }

        public Entity.MultiplePurposeRoom First(Expression<Func<Entity.MultiplePurposeRoom, bool>> predicate)
        {
            return _multiplePurposeRoomRepository.First(predicate);
        }

        public List<Entity.MultiplePurposeRoom> Get()
        {
            return _multiplePurposeRoomRepository.Get();
        }

        public Entity.MultiplePurposeRoom Get(int id)
        {
            return _multiplePurposeRoomRepository.First(q=> q.Id == id);
        }

        public List<Entity.MultiplePurposeRoom> Get(Expression<Func<Entity.MultiplePurposeRoom, bool>> predicate)
        {
            return _multiplePurposeRoomRepository.Get(predicate);
        }

        public void Update(Entity.MultiplePurposeRoom target)
        {
            throw new NotImplementedException();
        }
    }
}
