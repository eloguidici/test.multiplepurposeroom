﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Core.Business
{
    public class HolidayBusiness : IHolidayBusiness
    {
        private IHolidayRepository _holidayRepository;

        public HolidayBusiness(IHolidayRepository holidayRepository)
        {
            _holidayRepository = holidayRepository;
        }

        public void Add(Entity.Holiday target)
        {
            throw new NotImplementedException();
        }

        public bool Any(Expression<Func<Entity.Holiday, bool>> predicate)
        {
            return _holidayRepository.Any(predicate);
        }

        public Holiday First(Expression<Func<Holiday, bool>> predicate)
        {
            return _holidayRepository.First(predicate);
        }

        public List<Entity.Holiday> Get()
        {
            return _holidayRepository.Get();
        }

        public Entity.Holiday Get(int id)
        {
            return _holidayRepository.First(q=> q.Id == id);
        }

        public List<Entity.Holiday> Get(Expression<Func<Entity.Holiday, bool>> predicate)
        {
            return _holidayRepository.Get(predicate);
        }

        public void Update(Entity.Holiday target)
        {
            throw new NotImplementedException();
        }
    }
}
