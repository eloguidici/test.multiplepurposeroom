﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Test.MultiplePurposeRoom.Domain.Exceptions;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Core.Business
{
    public class UserBusiness : IUserBusiness
    {
        private IUserRepository _userRepository;

        public UserBusiness(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public void Add(User target)
        {
            throw new NotImplementedException();
        }

        public bool Any(Expression<Func<User, bool>> predicate)
        {
            return _userRepository.Any(predicate);
        }

        public User First(Expression<Func<User, bool>> predicate)
        {
            return _userRepository.First(predicate);
        }

        public List<User> Get()
        {
            return _userRepository.Get();
        }

        public User Get(int id)
        {
            return _userRepository.First(q => q.Id == id);
        }

        public List<User> Get(Expression<Func<User, bool>> predicate)
        {
            return _userRepository.Get(predicate);
        }

        public void Update(User target)
        {
            throw new NotImplementedException();
        }
    }
}
