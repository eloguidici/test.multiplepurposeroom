﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Test.MultiplePurposeRoom.Domain.Enums;
using Test.MultiplePurposeRoom.Domain.Exceptions;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Core.Business
{
    public class ReserveBusiness : IReserveBusiness
    {
        private IMultiplePurposeRoomBusiness _multiplePurposeRoomBusiness;
        private IHolidayBusiness _holidayBusiness;
        private IReserveRepository _reserveRepository;
        private ICustomerBusiness _customerBusiness;
        private IUserBusiness _userBusiness;

        public ReserveBusiness(IReserveRepository reserveRepository, IMultiplePurposeRoomBusiness multiplePurposeRoomBusiness,
            IHolidayBusiness holidayBusiness, ICustomerBusiness customerBusiness, IUserBusiness userBusiness)
        {
            _reserveRepository = reserveRepository;
            _multiplePurposeRoomBusiness = multiplePurposeRoomBusiness;
            _holidayBusiness = holidayBusiness;
            _customerBusiness = customerBusiness;
            _userBusiness = userBusiness;
        }

        public void Add(Reserve target)
        {
            //chequeamos si el usuario es valido
            var userIsValid = _userBusiness.Any(q => q.Id == target.UserId);
            if (!userIsValid)
            {
                throw new UserInvalidIdException();
            }

            //chequeamos si el salon es valido
            var room = _multiplePurposeRoomBusiness.Get(target.RoomId);
            if (room == null)
            {
                throw new MultiplePurposeRoomInvalidIdException();
            }

            //Chequeamos que la fecha que nos solicita no sea un feriado.
            var anyHoliday = _holidayBusiness.Any(q => q.Date == target.Date);
            if (anyHoliday)
            {
                throw new HolidayInvalidDateException();
            }


             var isRoomNotAvailable = _reserveRepository.Any(q =>
                                        (
                                            q.Status == (byte)ReserveStatus.Active
                                            &&
                                            q.MultiplePurposeRoom.TypeId == room.TypeId
                                            &&
                                            q.Date == target.Date    
                                            &&
                                            (
                                              (target.FromHour <= q.FromHour && target.ToHour >= q.ToHour)
                                              ||
                                              (target.FromHour > q.FromHour && target.FromHour < q.ToHour)
                                              ||
                                              (target.ToHour > q.FromHour && target.ToHour < q.ToHour)
                                            )
                                        )
            );

            if (isRoomNotAvailable)
            {
                throw new MultiplePurposeRoomNotAvailableException();
            }

            //agregamos el customer si no existe
            var anyCustomerRegistered = _customerBusiness.Any(q => q.Email == target.Customer.Email);
            if (!anyCustomerRegistered)
            {
                _customerBusiness.Add(target.Customer);
            }

            // agregamos la reserva
            target.CreatedDate = DateTime.UtcNow;
            target.Status = (byte)ReserveStatus.Active;
            _reserveRepository.Add(target);
        }

        public bool Any(Expression<Func<Reserve, bool>> predicate)
        {
            return _reserveRepository.Any(predicate);
        }

        public void Cancel(int reserveId)
        {
            var reserve = _reserveRepository.First(q => q.Id == reserveId);
            if (reserve == null)
            {
                throw new ReserveInvalidIdException();
            }

            if (reserve.Status == (byte)ReserveStatus.Cancel)
            {
                throw new ReserveStatusCanceledException();
            }

            if (reserve.Status == (byte)ReserveStatus.Finished)
            {
                throw new ReserveStatusFinishedException();
            }

            reserve.Status = (byte)ReserveStatus.Cancel;
            _reserveRepository.Update(reserve);
        }

        public Reserve First(Expression<Func<Reserve, bool>> predicate)
        {
            return _reserveRepository.First(predicate);
        }

        public List<Reserve> Get()
        {
            return _reserveRepository.Get();
        }

        public Reserve Get(int id)
        {
            return _reserveRepository.First(q => q.Id == id);
        }

        public List<Reserve> Get(Expression<Func<Reserve, bool>> predicate)
        {
            return _reserveRepository.Get(predicate);
        }

        public void Update(Reserve target)
        {
            //chequeamos si el usuario es valido
            var userIsValid = _userBusiness.Any(q => q.Id == target.UserId);
            if (!userIsValid)
            {
                throw new UserInvalidIdException();
            }

            //chequeamos si el salon es valido
            var room = _multiplePurposeRoomBusiness.Get(target.RoomId);
            if (room == null)
            {
                throw new MultiplePurposeRoomInvalidIdException();
            }

            //Chequeamos que la fecha que nos solicita no sea un feriado.
            var anyHoliday = _holidayBusiness.Any(q => q.Date == target.Date);
            if (anyHoliday)
            {
                throw new HolidayInvalidDateException();
            }

            var reserve = _reserveRepository.First(q => q.Id == target.Id);
            if (reserve == null)
            {
                throw new ReserveInvalidIdException();
            }

            var isRoomNotAvailable = _reserveRepository.Any(q =>
                                       (
                                           q.Id != target.Id
                                           &&
                                           q.Status == (byte)ReserveStatus.Active
                                           &&
                                           q.MultiplePurposeRoom.TypeId == room.TypeId
                                           &&
                                           q.Date == target.Date
                                           &&
                                           (
                                             (target.FromHour <= q.FromHour && target.ToHour >= q.ToHour)
                                             ||
                                             (target.FromHour > q.FromHour && target.FromHour < q.ToHour)
                                             ||
                                             (target.ToHour > q.FromHour && target.ToHour < q.ToHour)
                                           )
                                       )
           );

            if (isRoomNotAvailable)
            {
                throw new MultiplePurposeRoomNotAvailableException();
            }

            // actualizamos la reserva
            target.Status = (byte)ReserveStatus.Active;
            _reserveRepository.Update(target);
        }
    }
}
