﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Test.MultiplePurposeRoom.Domain.Exceptions;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Core.Business
{
    public class CustomerBusiness : ICustomerBusiness
    {
        private ICustomerRepository _customerRepository;

        public CustomerBusiness(ICustomerRepository CustomerRepository)
        {
            _customerRepository = CustomerRepository;
        }

        public void Add(Customer target)
        {
            if(String.IsNullOrEmpty(target.Email))
            {
                throw new CustomerInvalidEmailException();
            }

            _customerRepository.Add(target);
        }

        public bool Any(Expression<Func<Customer, bool>> predicate)
        {
            return _customerRepository.Any(predicate);
        }

        public Customer First(Expression<Func<Customer, bool>> predicate)
        {
            return _customerRepository.First(predicate);
        }

        public List<Customer> Get()
        {
            return _customerRepository.Get();
        }

        public Customer Get(int id)
        {
            return _customerRepository.First(q => q.Id == id);
        }

        public List<Customer> Get(Expression<Func<Customer, bool>> predicate)
        {
            return _customerRepository.Get(predicate);
        }

        public void Update(Customer target)
        {
            _customerRepository.Update(target);
        }
    }
}
