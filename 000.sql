﻿USE [master]
GO
/****** Object:  Database [TestMultiplePurposeRoom]    Script Date: 21-Apr-19 7:42:38 PM ******/
CREATE DATABASE [TestMultiplePurposeRoom]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Test', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Test.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Test_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Test_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestMultiplePurposeRoom].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET  MULTI_USER 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TestMultiplePurposeRoom]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Surname] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Equipment]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Equipment](
	[Id] [int] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Equipment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Holiday]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holiday](
	[Id] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Holiday] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultiplePurposeRoom]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultiplePurposeRoom](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[Capacity] [int] NOT NULL,
	[Dimension] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_MultiplePurposeRoom] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultiplePurposeRoomEquipment]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultiplePurposeRoomEquipment](
	[RoomId] [int] NOT NULL,
	[EquipmentId] [int] NOT NULL,
	[Count] [int] NOT NULL,
 CONSTRAINT [PK_MultiplePurposeRoomEquipment_1] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC,
	[EquipmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MultiplePurposeRoomType]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MultiplePurposeRoomType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_MultiplePurposeRoomType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reserve]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reserve](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FromHour] [int] NOT NULL,
	[ToHour] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Reserve_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 21-Apr-19 7:42:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Surname] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MultiplePurposeRoom]  WITH CHECK ADD  CONSTRAINT [FK_MultiplePurposeRoom_MultiplePurposeRoomType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[MultiplePurposeRoomType] ([Id])
GO
ALTER TABLE [dbo].[MultiplePurposeRoom] CHECK CONSTRAINT [FK_MultiplePurposeRoom_MultiplePurposeRoomType]
GO
ALTER TABLE [dbo].[MultiplePurposeRoomEquipment]  WITH CHECK ADD  CONSTRAINT [FK_MultiplePurposeRoomEquipment_Equipment] FOREIGN KEY([EquipmentId])
REFERENCES [dbo].[Equipment] ([Id])
GO
ALTER TABLE [dbo].[MultiplePurposeRoomEquipment] CHECK CONSTRAINT [FK_MultiplePurposeRoomEquipment_Equipment]
GO
ALTER TABLE [dbo].[MultiplePurposeRoomEquipment]  WITH CHECK ADD  CONSTRAINT [FK_MultiplePurposeRoomEquipment_MultiplePurposeRoom] FOREIGN KEY([RoomId])
REFERENCES [dbo].[MultiplePurposeRoom] ([Id])
GO
ALTER TABLE [dbo].[MultiplePurposeRoomEquipment] CHECK CONSTRAINT [FK_MultiplePurposeRoomEquipment_MultiplePurposeRoom]
GO
ALTER TABLE [dbo].[Reserve]  WITH CHECK ADD  CONSTRAINT [FK_Reserve_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Reserve] CHECK CONSTRAINT [FK_Reserve_Customer]
GO
ALTER TABLE [dbo].[Reserve]  WITH CHECK ADD  CONSTRAINT [FK_Reserve_MultiplePurposeRoom] FOREIGN KEY([RoomId])
REFERENCES [dbo].[MultiplePurposeRoom] ([Id])
GO
ALTER TABLE [dbo].[Reserve] CHECK CONSTRAINT [FK_Reserve_MultiplePurposeRoom]
GO
ALTER TABLE [dbo].[Reserve]  WITH CHECK ADD  CONSTRAINT [FK_Reserve_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Reserve] CHECK CONSTRAINT [FK_Reserve_User]
GO
USE [master]
GO
ALTER DATABASE [TestMultiplePurposeRoom] SET  READ_WRITE 
GO
