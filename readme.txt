Aclaraciones del test:

Tiempo de desarrollo empleado aproximado 10 hs aprox.

Utilice predicados en la capa de negocio, suponiendo que vamos a conectarnos con la base a trav�s de un ORM.

Para no hacer conversiones innecesarias manejo la hora en formato militar Ej: 1400 => 14hs

Utilizo las fechas en formato UTC asumiendo que el sistema se puede hostear o usar en otras latitudes.

Tema de security, aplico filters (annotations) sin logica para asumir que hay un flujo de autenticacion y autorizacion contra una api se seguridad.

Hay validaciones que son propias de frontend, por eso no las tengo en cuenta en el c�digo, por ejemplo: regular expression que valide que un email esta bien formado, fechas de reserva en el pasado, etc.

El campo dimensi�n de la sala, lo asumo como metro cuadrado.

Asumo que las salas ya tienen definidos los equipamientos, porque lo que me centro en la reserva del sal�n.
Tambien se podria hacer que cada reserva incluya un sal�n y una serie de equipamientos propia del tipo de sala.(Lo deje para lo �ltimo, y no me dieron los tiempos)

Tanto en los controllers de MVC como de la API, los objetos que recibe deber�an ser DTOs, y luego mapearlos con los objetos de negocio.

Utilice First base de Entity Framework, por un tema de tiempos, quedando relacionado el proyecto de entidades y repositorios.  

Utilice Ninject para inyecci�n de dependencia, si necesit�ramos cambiar la forma en que se implementa el acceso a datos, solo tendr�amos que cambiar lo que le inyectamos a las clases business, con un nuevo repositorio, por ejemplo, que se conecte con Couchbase o SQL server a traves de Ado.net

Utilice AutoMapper para el mapeo de entidades de negocios a un dto hacia el front.

Tanto Ninject como Automapper los implemente tanto en MVC como en Api, ya que la implementaci�n puede ser diferente para cada uno.
Por ejemplo, MVC podr�a tener DTOs muchos mas ricos en informaci�n, ya que son los que ayudan a renderizar las p�ginas, y tanto el repositorio puede ser que vaya contra una base de datos, mientras que el repositorio que usa la API podr�a ir contra una base de datos NoSql, lo mismo con los DTOs, que podr�an ser m�s livianos (flyweight)

Los mensajes de las excepciones los maneje como una constante, aunque por un tema de multi idioma podr�amos utilizar un archivo de recursos(resx).

Los test unitarios, los aplique a la clase ReserveBusiness, que es la que tiene la mayor�a de las operaciones.

Genere script iniciales de la creaci�n de la base y de datos cat�logo.
Se podr�a haber un proyecto de base de datos, y generar script post deploy autom�ticos de las tablas catalogo.

Patrones de dise�o que se pueden utilizar y como:
Observer => Cuando hay un cambio en el modelo, podr�amos comunicar  a todos los usuarios conectados, disparando un evento que sea capturado por un hub y comunicado al front por medio de SignalR por ejemplo.
Decorator => El salon, los maneje con un campo tipo, pero tambien se podria pensar con una clase base, y luego ir decorando de acuerdo a lo que ofrece cada una.
Singleton => lo usa Ninject internamente