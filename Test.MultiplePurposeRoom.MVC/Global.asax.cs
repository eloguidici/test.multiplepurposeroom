﻿using AutoMapper;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Test.MultiplePurposeRoom.DTO;

namespace Test.MultiplePurposeRoom.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            System.Web.Mvc.DependencyResolver.SetResolver(new NinjectResolver());

            Mapper.Initialize(cfg => {
                cfg.CreateMap<Entity.Holiday, HolidayDto>();
                cfg.CreateMap<Entity.MultiplePurposeRoom, MultiplePurposeRoomDto>();
                cfg.CreateMap<Entity.Customer, CustomerDto>();
                cfg.CreateMap<Entity.Reserve, ReserveDto>();
                cfg.CreateMap<Entity.MultiplePurposeRoomType, MultiplePurposeRoomTypeDto>();
                cfg.CreateMap<Entity.MultiplePurposeRoomEquipment, MultiplePurposeRoomEquipmentDto>();
                cfg.CreateMap<Entity.Equipment,EquipmentDto>();
                cfg.CreateMap<Entity.User, UserDto>();
            });

        }
    }
}
