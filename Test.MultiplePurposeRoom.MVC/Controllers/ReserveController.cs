﻿using System.Web.Mvc;
using Test.MultiplePurposeRoom.Domain.Interfaces;

namespace Test.MultiplePurposeRoom.MVC.Controllers
{
    public class ReserveController : Controller
    {
        IReserveBusiness _business;

        public ReserveController(IReserveBusiness business)
        {
            _business = business;
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Details(int id)
        {
            var model = _business.Get(id);
            return View(model);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Entity.Reserve target)
        {
            try
            {
                _business.Add(target);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Update(int id)
        {
            var model = _business.Get(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(int id, Entity.Reserve target)
        {
            try
            {
                _business.Update(target);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            var model = _business.Get(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Cancel(int id)
        {
            try
            {
                _business.Cancel(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


    }
}
