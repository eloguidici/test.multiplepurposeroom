﻿using System.Web.Mvc;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.DTO;

namespace Test.MultiplePurposeRoom.MVC.Controllers
{
    public class MultiplePurposeRoomController : Controller
    {
        IMultiplePurposeRoomBusiness _business;

        public MultiplePurposeRoomController(IMultiplePurposeRoomBusiness business)
        {
            _business = business;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            var model = AutoMapper.Mapper.Map<MultiplePurposeRoomDto>(_business.Get(id));
            return View("details",model);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(Entity.MultiplePurposeRoom target)
        {
            try
            {
                _business.Add(target);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

         public ActionResult Update(int id)
        {
            var model = AutoMapper.Mapper.Map<MultiplePurposeRoomDto>(_business.Get(id));
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(int id, Entity.MultiplePurposeRoom target)
        {
            try
            {
                _business.Update(target);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
