﻿using Ninject;
using System;
using System.Collections.Generic;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.Repository;

namespace Test.MultiplePurposeRoom.MVC
{

    public class NinjectResolver : System.Web.Mvc.IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectResolver()
        {
            _kernel = new StandardKernel();
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            //business
            _kernel.Bind<IMultiplePurposeRoomBusiness>().To<Core.Business.MultiplePurposeRoomBusiness>().InSingletonScope();
            _kernel.Bind<IHolidayBusiness>().To<Core.Business.HolidayBusiness>().InSingletonScope();
            _kernel.Bind<IReserveBusiness>().To<Core.Business.ReserveBusiness>().InSingletonScope();
            _kernel.Bind<ICustomerBusiness>().To<Core.Business.CustomerBusiness>().InSingletonScope();
            _kernel.Bind<IUserBusiness>().To<Core.Business.UserBusiness>().InSingletonScope();

            //repository
            _kernel.Bind<IMultiplePurposeRoomRepository>().To<MultiplePurposeRoomRepository>().InSingletonScope();
            _kernel.Bind<IHolidayRepository>().To<HolidayRepository>().InSingletonScope();
            _kernel.Bind<IReserveRepository>().To<ReserveRepository>().InSingletonScope();
            _kernel.Bind<ICustomerRepository>().To<CustomerRepository>().InSingletonScope();
            _kernel.Bind<IUserRepository>().To<UserRepository>().InSingletonScope();
        }
    }
}