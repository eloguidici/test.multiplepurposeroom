﻿USE [TestMultiplePurposeRoom]
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (1, N'Parlante 1500 w')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (2, N'Consola DJ')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (3, N'Pantalla')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (4, N'Maquin de Humo')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (5, N'Luces')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (6, N'Bateria ')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (7, N'Guitarra')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (8, N'Bajo')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (9, N'Parlante 750w')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (10, N'Red de arco')
GO
INSERT [dbo].[Equipment] ([Id], [Description]) VALUES (11, N'Pelota')
GO
SET IDENTITY_INSERT [dbo].[MultiplePurposeRoomType] ON 
GO
INSERT [dbo].[MultiplePurposeRoomType] ([Id], [Description]) VALUES (1, N'Sala de ensayo')
GO
INSERT [dbo].[MultiplePurposeRoomType] ([Id], [Description]) VALUES (2, N'Salón de fiesta')
GO
INSERT [dbo].[MultiplePurposeRoomType] ([Id], [Description]) VALUES (3, N'Cancha de futbol')
GO
SET IDENTITY_INSERT [dbo].[MultiplePurposeRoomType] OFF
GO
SET IDENTITY_INSERT [dbo].[MultiplePurposeRoom] ON 
GO
INSERT [dbo].[MultiplePurposeRoom] ([Id], [TypeId], [Capacity], [Dimension]) VALUES (1, 1, 100, CAST(300 AS Decimal(18, 0)))
GO
INSERT [dbo].[MultiplePurposeRoom] ([Id], [TypeId], [Capacity], [Dimension]) VALUES (2, 2, 50, CAST(200 AS Decimal(18, 0)))
GO
INSERT [dbo].[MultiplePurposeRoom] ([Id], [TypeId], [Capacity], [Dimension]) VALUES (3, 3, 75, CAST(250 AS Decimal(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[MultiplePurposeRoom] OFF
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (1, 1, 2)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (1, 6, 1)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (1, 7, 2)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (2, 1, 4)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (2, 2, 1)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (2, 3, 1)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (2, 4, 1)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (2, 5, 10)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (3, 10, 2)
GO
INSERT [dbo].[MultiplePurposeRoomEquipment] ([RoomId], [EquipmentId], [Count]) VALUES (3, 11, 2)
GO
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([Id], [Name], [Surname], [Email]) VALUES (1, N'Emiliano', N'Loguidici', N'emilianologuidici@gmail.com')
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 
GO
INSERT [dbo].[Customer] ([Id], [Name], [Surname], [Email]) VALUES (1, N'John', N'Doe', N'john@doe.com')
GO
INSERT [dbo].[Customer] ([Id], [Name], [Surname], [Email]) VALUES (2, N'Mary', N'Jane', N'mary@jane.com')
GO
INSERT [dbo].[Customer] ([Id], [Name], [Surname], [Email]) VALUES (4, N'Mary', N'Foster', N'mary@Foster.com')
GO
INSERT [dbo].[Customer] ([Id], [Name], [Surname], [Email]) VALUES (5, N'Mary', N'Foster', N'mary@Foster.com')
GO
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Reserve] ON 
GO
INSERT [dbo].[Reserve] ([Id], [FromHour], [ToHour], [UserId], [RoomId], [CustomerId], [Status], [CreatedDate], [Date]) VALUES (2, 1200, 1300, 1, 1, 1, 1, CAST(N'2019-04-21T00:00:00.000' AS DateTime), CAST(N'2019-04-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Reserve] ([Id], [FromHour], [ToHour], [UserId], [RoomId], [CustomerId], [Status], [CreatedDate], [Date]) VALUES (1002, 1100, 1500, 1, 1, 5, 1, CAST(N'2019-04-21T22:38:34.850' AS DateTime), CAST(N'2019-04-22T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Reserve] OFF
GO
INSERT [dbo].[Holiday] ([Id], [Date], [Description]) VALUES (1, CAST(N'2019-12-25T00:00:00.000' AS DateTime), N'Navidad')
GO
INSERT [dbo].[Holiday] ([Id], [Date], [Description]) VALUES (2, CAST(N'2019-12-08T00:00:00.000' AS DateTime), N'Dia de la Virgen')
GO
