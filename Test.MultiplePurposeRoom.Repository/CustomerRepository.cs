﻿using Test.MultiplePurposeRoom.Domain.Interfaces;

namespace Test.MultiplePurposeRoom.Repository
{
    public class CustomerRepository : AbstractRepository<Entity.Customer>, ICustomerRepository
    {
        public CustomerRepository() : base() { }
    }
}
