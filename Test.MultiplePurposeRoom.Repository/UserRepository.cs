﻿using Test.MultiplePurposeRoom.Domain.Interfaces;

namespace Test.MultiplePurposeRoom.Repository
{
    public class UserRepository : AbstractRepository<Entity.User>, IUserRepository
    {
        public UserRepository() : base() { }
    }
}
