﻿using Test.MultiplePurposeRoom.Domain.Interfaces;

namespace Test.MultiplePurposeRoom.Repository
{
    public class MultiplePurposeRoomRepository : AbstractRepository<Entity.MultiplePurposeRoom>, IMultiplePurposeRoomRepository
    {
        public MultiplePurposeRoomRepository() : base() { }
    }
}
