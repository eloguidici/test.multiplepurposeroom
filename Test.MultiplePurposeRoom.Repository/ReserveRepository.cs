﻿using Test.MultiplePurposeRoom.Domain.Interfaces;

namespace Test.MultiplePurposeRoom.Repository
{
    public class ReserveRepository : AbstractRepository<Entity.Reserve>, IReserveRepository
    {
        public ReserveRepository() : base() { }
    }
}
