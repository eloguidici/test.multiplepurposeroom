﻿using Test.MultiplePurposeRoom.Domain.Interfaces;

namespace Test.MultiplePurposeRoom.Repository
{
    public class HolidayRepository : AbstractRepository<Entity.Holiday>, IHolidayRepository
    {
        public HolidayRepository() : base() { }
    }
}
