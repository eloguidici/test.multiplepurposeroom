﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Repository
{
    public abstract class AbstractRepository<T> where T : class
    {
        public DbContext _context;

        public AbstractRepository()
        {
            _context = new TestMultiplePurposeRoomEntities();
            // _context.Configuration.ProxyCreationEnabled = false;
            // _context.Configuration.LazyLoadingEnabled = true;
        }
        public void Add(T target)
        {
            try
            {
                _context.Set<T>().Add(target);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Any(predicate);
        }

        public T First(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().FirstOrDefault(predicate);
        }

        public List<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate).ToList();
        }


        private IEnumerable<T> Sort(IEnumerable<T> source, string sortBy, string sortDirection = "desc")
        {
            var param = Expression.Parameter(typeof(T), "item");

            var sortExpression = Expression.Lambda<Func<T, object>>
                (Expression.Convert(Expression.Property(param, sortBy), typeof(object)), param);

            switch (sortDirection.ToLower())
            {
                case "asc":
                    return source.AsQueryable<T>().OrderBy<T, object>(sortExpression);
                default:
                    return source.AsQueryable<T>().OrderByDescending<T, object>(sortExpression);

            }
        }


        //public virtual Paged<T> Get(Expression<Func<T, bool>> predicate, int skip, int take)
        //{
        //    var target = _context.Set<T>().Where(predicate).ToList();

        //    var pages = ((target.Count - 1) / take) + 1;
        //    return new Paged<T>()
        //    {
        //        Target = target.Skip(skip).Take(take).ToList(),
        //        Count = target.Count,
        //        Pages = ((target.Count - 1) / take) + 1
        //    };
        //}


        public void Update(T target)
        {
            try
            {
                _context.Set<T>().AddOrUpdate(target);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> Get()
        {
            return _context.Set<T>().ToList();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
