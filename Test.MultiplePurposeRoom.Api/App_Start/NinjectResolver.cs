﻿using Ninject;
using Ninject.Extensions.ChildKernel;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http.Dependencies;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.Repository;

namespace Test.MultiplePurposeRoom.Api
{
    public class NinjectResolver : IDependencyResolver
    {
        private IKernel _kernel;

        public NinjectResolver() : this(new StandardKernel())
        {

        }

        public NinjectResolver(IKernel ninjectKernel, bool scope = false)
        {
            _kernel = ninjectKernel;
            if (!scope)
            {
                AddBindings(_kernel);
            }
        }

        private void AddBindings(IKernel kernel)
        {
            // singleton and transient bindings go here
        }

        private IKernel AddRequestBindings(IKernel kernel)
        {
            //business
            kernel.Bind<IMultiplePurposeRoomBusiness>().To<Core.Business.MultiplePurposeRoomBusiness>().InSingletonScope();
            kernel.Bind<IHolidayBusiness>().To<Core.Business.HolidayBusiness>().InSingletonScope();
            kernel.Bind<IReserveBusiness>().To<Core.Business.ReserveBusiness>().InSingletonScope();
            kernel.Bind<ICustomerBusiness>().To<Core.Business.CustomerBusiness>().InSingletonScope();
            kernel.Bind<IUserBusiness>().To<Core.Business.UserBusiness>().InSingletonScope();

            //repository
            kernel.Bind<IMultiplePurposeRoomRepository>().To<MultiplePurposeRoomRepository>().InSingletonScope();
            kernel.Bind<IHolidayRepository>().To<HolidayRepository>().InSingletonScope();
            kernel.Bind<IReserveRepository>().To<ReserveRepository>().InSingletonScope();
            kernel.Bind<ICustomerRepository>().To<CustomerRepository>().InSingletonScope();
            kernel.Bind<IUserRepository>().To<UserRepository>().InSingletonScope();

            return kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectResolver(AddRequestBindings(new ChildKernel(_kernel)), true);
        }

        public void Dispose()
        {
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

    }
}