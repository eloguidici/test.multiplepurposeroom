﻿using Ninject;
using System.Web.Http;
using Test.MultiplePurposeRoom.Api;

namespace Test.MultiplePurposeRoom.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling
                                            = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            //config.DependencyResolver = new NinjectResolver();
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver();
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "api/{controller}/{id}",
                    defaults: new { id = RouteParameter.Optional }
                );
        }


    }
}
