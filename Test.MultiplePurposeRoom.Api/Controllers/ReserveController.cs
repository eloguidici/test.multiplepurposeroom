﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.DTO;
using Test.MultiplePurposeRoom.Entity;

namespace Test.MultiplePurposeRoom.Api.Controllers
{
    [RoutePrefix("api/Reserve")]
    public class ReserveController : ApiController
    {
        IReserveBusiness _business;

        public ReserveController(IReserveBusiness business)
        {
            _business = business;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Post(Reserve target)
        {
            try
            {
                _business.Add(target);
            }
            catch (HttpException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok();
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult Put(Reserve target)
        {
            try
            {
                _business.Update(target);
            }
            catch (HttpException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok();
        }

        [HttpPatch]
        [Route("cancel/{reserveId:int}")]
        public IHttpActionResult Patch(int reserveId)
        {
            try
            {
                _business.Cancel(reserveId);
            }
            catch (HttpException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok();
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            List<ReserveDto> list;

            try
            {
                list = AutoMapper.Mapper.Map<List<ReserveDto>>(_business.Get());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok(list);
        }

        [Route("{reserveId:int}")]
        [HttpGet]
        public IHttpActionResult Get(int reserveId)
        {
            ReserveDto reserve;

            try
            {
                reserve = AutoMapper.Mapper.Map<ReserveDto>(_business.Get(reserveId));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok(reserve);
        }

    }
}
