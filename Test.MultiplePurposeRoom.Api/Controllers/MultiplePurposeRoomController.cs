﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Test.MultiplePurposeRoom.Domain.Interfaces;
using Test.MultiplePurposeRoom.DTO;
using Test.MultiplePurposeRoom.Api.Filters;
using System.Web;

namespace Test.MultiplePurposeRoom.Api.Controllers
{
    [AuthorizationFilter]
    [RoutePrefix("api/MultiplePurposeRoom")]
    public class MultiplePurposeRoomController : ApiController
    {
        IMultiplePurposeRoomBusiness _business;

        public  MultiplePurposeRoomController(IMultiplePurposeRoomBusiness business)
        {
            _business = business;
        }

        // POST api/MultiplePurposeRoom 
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post(Entity.MultiplePurposeRoom target)
        {
            try
            {
                _business.Add(target);
            }
            catch (HttpException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok();
        }

        // PUT api/MultiplePurposeRoom 
        [Route("")]
        [HttpPut]
        public IHttpActionResult Put(Entity.MultiplePurposeRoom target)
        {
            try
            {
                _business.Update(target);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok();
        }

        // GET api/MultiplePurposeRoom 
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            List<MultiplePurposeRoomDto> list;
            try
            {
                list = AutoMapper.Mapper.Map<List<MultiplePurposeRoomDto>>(_business.Get());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok(list);
        }

    }
}
