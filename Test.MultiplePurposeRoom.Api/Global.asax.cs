﻿using AutoMapper;
using System.Web.Http;
using Test.MultiplePurposeRoom.DTO;

namespace Test.MultiplePurposeRoom.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
                        
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Entity.Holiday, HolidayDto>();
                cfg.CreateMap<Entity.MultiplePurposeRoom, MultiplePurposeRoomDto>();
                cfg.CreateMap<Entity.Customer, CustomerDto>();
                cfg.CreateMap<Entity.Reserve, ReserveDto>();
                cfg.CreateMap<Entity.MultiplePurposeRoomType, MultiplePurposeRoomTypeDto>();
                cfg.CreateMap<Entity.MultiplePurposeRoomEquipment, MultiplePurposeRoomEquipmentDto>();
                cfg.CreateMap<Entity.Equipment,EquipmentDto>();
                cfg.CreateMap<Entity.User, UserDto>();
            });

        }


    }
}
